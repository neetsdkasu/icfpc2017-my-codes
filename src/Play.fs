// Try ICFPContest2017
// Game play module
// author: Leonardone @ NEETSDKASU
module Play

open neetsdkasu

let setup json =
    let punter = Json.find "punter" json
    let ps     = Json.find "punters" json
    let (futures, splurge, options) =
        match Json.tryFind "settings" json with
        | None    -> (false, false, false)
        | Some st -> let f = Json.containsKey "futures" st
                     let s = Json.containsKey "splurge" st
                     let o = Json.containsKey "options" st
                     (f, s, o)
    let data   = Json.find "map" json
    let sites  = Json.find "sites" data
                 |> Json.getArr
                 |> Array.map (Json.find "id")
                 |> Json.arr
    let rivers = Json.find "rivers" data
                 |> Json.getArr
                 |> Array.collect (fun rv ->
                        let s = Json.find "source" rv
                        let t = Json.find "target" rv
                        if Json.getNum s < Json.getNum t
                            then [| s ; t |]
                            else [| t ; s |]
                    )
                 |> Json.arr
    let rvcount  = Array.length (Json.getArr rivers) / 2
    let ptrivers = Array.create rvcount ps
    let pass     = Array.create (int <| Json.getNum ps) (Json.num 0) 
    let ftrivers = Array.empty
    let oprivers = if options then Array.create rvcount ps
                              else Array.empty
    let state = data
                |> Json.add "turn"     (Json.num 0)
                |> Json.add "pass"     (Json.arr pass)
                |> Json.add "punter"   punter
                |> Json.add "ptcount"  ps
                |> Json.add "rvcount"  (Json.num rvcount)
                |> Json.add "ptrivers" (Json.arr ptrivers)
                |> Json.add "futures"  (Json.bln futures)
                |> Json.add "ftrivers" (Json.arr ftrivers)
                |> Json.add "splurge"  (Json.bln splurge)
                |> Json.add "options"  (Json.bln options)
                |> Json.add "oprivers" (Json.arr oprivers)
                |> Json.add "sites"    sites
                |> Json.add "rivers"   rivers
                |> Json.add "alongs"   (Json.arr Array.empty)
    Json.empty
    |> Json.add "ready" punter
    |> if futures
            then Json.add "futures" (Json.arr ftrivers)
            else id
    |> Json.add "state" state
    |> Json.toString

type River = { s: int; t: int }

let private initMaps rvcount rivers =
    seq { 0 .. rvcount-1 }
    |> Seq.fold (fun (m1, m2) i ->
            let s = int << Json.getNum <| Array.get rivers (i*2)
            let t = int << Json.getNum <| Array.get rivers (i*2+1)
            let m1x = Map.add { s = s; t = t } i m1
            let m2p = m2 |> match Map.tryFind s m2 with
                            | None    -> Map.add s [t]
                            | Some ts -> Map.add s (t::ts)
            let m2x = m2p |> match Map.tryFind t m2p with
                             | None    -> Map.add t [s]
                             | Some ss -> Map.add t (s::ss)
            (m1x, m2x)
        ) (Map.empty, Map.empty)

let private updateClaim mv rvmap ptrivers =
    let dt = Json.find "claim"  mv
    let pi = Json.find "punter" dt
    let sc = Json.getNum <| Json.find "source" dt
    let tg = Json.getNum <| Json.find "target" dt
    let (s, t) = min sc tg, max sc tg
    let ky = { s = int s; t = int t }
    match Map.tryFind ky rvmap with
    | None   -> ()
    | Some i -> Array.set ptrivers i pi

let private updateOption mv rvmap oprivers =
    let dt = Json.find "option" mv
    let pi = Json.find "punter" dt
    let sc = Json.getNum <| Json.find "source" dt
    let tg = Json.getNum <| Json.find "target" dt
    let (s, t) = min sc tg, max sc tg
    let ky = { s = int s; t = int t }
    match Map.tryFind ky rvmap with
    | None   -> ()
    | Some i -> Array.set oprivers i pi

let private updateSplurge mv rvmap pass 
                          ptcount  ptrivers
                          options  oprivers =
    let dt = Json.find "splurge" mv
    let pi = Json.find "punter"  dt
    let rt = Json.getArr <| Json.find "route" dt
    let ln = Array.length rt
    let i  = int <| Json.getNum pi
    (int << Json.getNum <| Array.get pass i) - ln
    |> Json.num
    |> Array.set pass i
    seq { 1 .. ln-1 }
    |> Seq.iter (fun j ->
            let r1 = Json.getNum <| Array.get rt j
            let r2 = Json.getNum <| Array.get rt (j-1)
            let (s, t) = min r1 r2, max r1 r2
            let ky = { s = int s; t = int t }
            match Map.tryFind ky rvmap with
            | None   -> ()
            | Some p ->
                let y = int << Json.getNum
                            <| Array.get ptrivers p
                if y = ptcount then
                    Array.set ptrivers p pi
                elif options then
                    let z = int << Json.getNum
                                <| Array.get oprivers p
                    if z = ptcount then
                        Array.set oprivers p pi
                    else ()
                else ()
        )

let private updatePass mv pass =
    let dt = Json.find "pass"   mv
    let pi = Json.find "punter" dt
    let i  = int <| Json.getNum pi
    (int << Json.getNum <| Array.get pass i) + 1
    |> Json.num
    |> Array.set pass i


let private update rvmap pass
                   ptcount ptrivers
                   options oprivers =
    Array.iter (fun mv ->
            let act = List.tryFind
                        ((|>) mv << Json.containsKey)
                        ["claim"; "pass"; "splurge"; "option"]
            match act with
            | Some "pass"    -> updatePass    mv pass
            | Some "claim"   -> updateClaim   mv rvmap ptrivers
            | Some "option"  -> updateOption  mv rvmap oprivers
            | Some "splurge" -> updateSplurge mv rvmap pass
                                              ptcount  ptrivers
                                              options  oprivers
            | _ -> ()
        )

let private addAlongs alongs d x =
    if Array.length alongs = d then
        Array.append alongs [| Json.arr [|Json.num x|] |]
    else
        let ar = Json.getArr <| Array.get alongs d
        let nw = Array.append ar [|Json.num x|]
        Array.set alongs d <| Json.arr nw
        alongs

let private goNewJourney mines rvmap edmap alongs
                         ptcount ptrivers options oprivers =
    mines
    |> Array.tryPick (fun mn ->
            let p = int <| Json.getNum mn
            match Map.tryFind p edmap with
            | None    -> None
            | Some xs ->
                xs
                |> List.tryPick (fun x ->
                        let ky = { s = min p x; t = max p x }
                        match Map.tryFind ky rvmap with
                        | None   -> None
                        | Some i ->
                            let y = int << Json.getNum 
                                        <| Array.get ptrivers i
                            if y = ptcount then
                                Some(ky, p, x, false)
                            elif options then
                                let z = int << Json.getNum
                                            <| Array.get oprivers i
                                if z = ptcount
                                    then Some(ky, p, x, true)
                                    else None
                            else None
                    )
        )
    |> function | Some(ky, p, x, opt) ->
                        let a = addAlongs alongs 0 p
                        (ky, addAlongs a 1 x, opt)
                | None ->
                    rvmap
                    |> Map.tryPick (fun ky i ->
                            let y = int << Json.getNum
                                        <|Array.get ptrivers i
                            if y = ptcount then
                                Some(ky, false)
                            elif options then
                                let z = int << Json.getNum
                                            <| Array.get oprivers i
                                if z = ptcount
                                    then Some(ky, true)
                                    else None
                            else None
                        )
                    |> function | Some(ky, opt) -> (ky, alongs, opt)
                                | None -> ({ s = 0; t = 0 }, alongs, false)

let private goJourney mines rvmap edmap alongs
                      ptcount ptrivers options oprivers =
    let ln = Array.length alongs
    seq { for i in ln-1 .. -1 .. 0 -> (i, Array.get alongs i) }
    |> Seq.tryPick (fun tp ->
            let (d, ar) = tp
            Json.getArr ar
            |> Array.tryPick (fun a ->
                    let p = int << Json.getNum <| a
                    match Map.tryFind p edmap with
                    | None    -> None
                    | Some xs ->
                        xs
                        |> List.tryPick (fun x ->
                                let ky = { s = min p x; t = max p x }
                                match Map.tryFind ky rvmap with
                                | None   -> None
                                | Some i ->
                                    let y = int << Json.getNum
                                                <| Array.get ptrivers i
                                    if y = ptcount then
                                        Some(ky, d+1, x, false)
                                    elif options then
                                        let z = int << Json.getNum
                                                    <| Array.get oprivers i
                                        if z = ptcount
                                            then Some(ky, d+1, x, true)
                                            else None
                                    else None
                            )
                )
        )
    |> function | Some(ky, d, x, opt) -> (ky, addAlongs alongs d x, opt)
                | None -> goNewJourney mines rvmap edmap alongs
                                       ptcount ptrivers options oprivers
                
        
let gamePlay json =
    let moves    = Json.getArr << Json.find "moves" <| Json.find "move" json
    let state    = Json.find "state"  json
    let punterId = Json.find "punter" state
    let ptcount  = int << Json.getNum <| Json.find "ptcount" state
    let rvcount  = int << Json.getNum <| Json.find "rvcount" state
    let turn     = int << Json.getNum <| Json.find "turn"    state
    let rivers   = Json.getArr <| Json.find "rivers"   state
    let mines    = Json.getArr <| Json.find "mines"    state
    let ptrivers = Json.getArr <| Json.find "ptrivers" state
    let oprivers = Json.getArr <| Json.find "oprivers" state
    let options  = Json.getBln <| Json.find "options"  state
    let splurge  = Json.getBln <| Json.find "splurge"  state
    let pass     = Json.getArr <| Json.find "pass"     state
    let alongs   = Json.getArr <| Json.find "alongs"   state
    let (rvmap, edmap) = initMaps rvcount rivers
    update rvmap pass ptcount ptrivers options oprivers moves
    let go = if Array.length alongs = 0 then goNewJourney else goJourney
    let (ky, alongs, opt) = go mines rvmap edmap alongs 
                               ptcount ptrivers options oprivers
    let state = state
                |> Json.add "turn"   (Json.num (turn+1))
                |> Json.add "alongs" (Json.arr alongs)
    Json.empty
    |> if ky.s = ky.t then
           Json.add "pass" (Json.add "punter" punterId Json.empty)
       else
           let cmd = if opt then "option" else "claim"
           Json.add cmd (
                   Json.empty
                   |> Json.add "punter" punterId
                   |> Json.add "source" (Json.num ky.s)
                   |> Json.add "target" (Json.num ky.t)
               )
    |> Json.toString
    |> (+)
    |> (|>) (Json.empty
             |> Json.add "state" state
             |> Json.toString
        )


let scoring json =
    let result = Json.find "stop" json
    let scores = Json.find "scores" result
    eprintfn "%A" scores