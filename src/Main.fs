// Try ICFPContest2017
// Main module
// author: Leonardone @ NEETSDKASU
module Main

open neetsdkasu
open Play

let private input() =
    let s = stdin.ReadToEnd()
    if s.[0] = '{' then
        (0, s)
    else
        let i = s.IndexOf(':')
        (i+1, s)

let private output s =
    stdout.WriteLine("{0}:{1}", String.length s, s)
    stdout.Flush()

let private getMessage() =
    // me
    Json.empty
    |> Json.add "me" (Json.str "neetsdkasu")
    |> Json.toString
    |> output
    // you
    let (i, s) = input()
    let (j, _) = Json.parse s i
    // get message
    let (k, t) = if j < String.length s
                     then (j, s)
                     else input()
    snd <| Json.parse t k
   
[<EntryPoint>]
let main _ =
    try
        let json = getMessage()
        
        let protocol = List.tryFind
                        ((|>) json << Json.containsKey)
                        ["punter"; "move"; "stop"]
        
        match protocol with
        | Some "punter" -> setup    json |> output
        | Some "move"   -> gamePlay json |> output
        | Some "stop"   -> scoring  json
        | _             -> ()
        
    with
        e -> eprintfn "%A" e
    0