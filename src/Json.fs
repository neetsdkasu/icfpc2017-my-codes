// JSON Parser
// author: Leonardone @ NEETSDKASU
//
// supported:
//  - string without backslash
//  - number
//  - array
//  - object
//  - boolean
//  - null
// non support:
//  - backslash
//  - invalid format
//
module neetsdkasu.Json

type JsonObject = Ar of JsonObject array
                | Ob of Map<string, JsonObject>
                | Nm of float
                | St of string
                | Bl of bool
                | Nul

let nul   = Nul
let bln b = Bl b
let str s = St s
let num n = Nm (float n)
let arr a = Ar a
let obj o = Ob o
let empty = Ob Map.empty
let add k v =
    function | Ob m -> Ob (Map.add k v m)
             | _    -> failwith "not object"
let find k =
    function | Ob m -> Map.find k m
             | _    -> failwith "not object"
let tryFind k =
    function | Ob m -> Map.tryFind k m
             | _    -> failwith "not object"
let containsKey k =
    function | Ob m -> Map.containsKey k m
             | _    -> failwith "not object"

let getBln = function | Bl b -> b
                      | _    -> failwith "not boolean"
let getStr = function | St s -> s
                      | _    -> failwith "not string"
let getNum = function | Nm n -> n
                      | _    -> failwith "not number"
let getArr = function | Ar a -> a
                      | _    -> failwith "not array"
let getObj = function | Ob o -> o
                      | _    -> failwith "not object"

let toString json =
    let sb = new System.Text.StringBuilder()
    let rec loop j =
        ignore <|
        match j with
        | Ar ar -> loopAr ar
        | Ob ob -> loopOb ob
        | Nm nm -> sb.Append(nm)
        | St st -> sb.Append('"').Append(st).Append('"')
        | Bl bl -> sb.Append(bl.ToString().ToLower())
        | Nul   -> sb.Append("null")
    and loopAr ar =
        let _ = sb.Append('[')
        Array.iteri (fun i a ->
            let _ = if i > 0 then sb.Append(',') else sb
            loop a
        ) ar
        sb.Append(']')   
    and loopOb ob =
        let _ = sb.Append('{')
        Map.toSeq ob
        |> Seq.iteri (fun i kv ->
                let _ = if i > 0 then sb.Append(',') else sb
                let _ = sb.Append('"')
                            .Append(fst kv)
                            .Append('"')
                            .Append(':')
                loop (snd kv)
           )
        sb.Append('}')
    loop json
    sb.ToString()

let private parseString (tx:string) p =
    let i = tx.IndexOf('"', p)
    if i < 0 then
        failwith "parse error"
    else
        (i+1, St (tx.Substring(p, i-p)))

let private getTokenPos (tx:string) p =
    let rec loop i =
        if i >= String.length tx then
            i
        else
            match tx.[i] with
            | ',' | ']' | '}' -> i
            | ch when System.Char.IsWhiteSpace(ch) -> i
            | _ -> loop (i+1)
    loop p

let private parseNumber (tx:string) p =
    let i = getTokenPos tx p
    let value =
        try float <| tx.Substring(p, i - p)
        with e -> failwith <| sprintf "parse error: %A" e
    (i, Nm value)

let private parseBoolean (tx:string) p =
    let i = getTokenPos tx p
    match tx.Substring(p, i - p) with
    | "true"  -> (i, Bl true)
    | "false" -> (i, Bl false)
    | _       -> failwith <| sprintf "parse error: (invalid boolean) [%d]" p    

let private parseNull (tx:string) p =
    let i = getTokenPos tx p
    match tx.Substring(p, i - p) with
    | "null"  -> (i, Nul)
    | _       -> failwith <| sprintf "parse error: (invalid null) [%d]" p   
    
let rec parse (tx:string) p =
    let rec loop i =
        if i >= String.length tx then
            failwith "parse error"
        else
            match tx.[i] with
            | '[' -> parseArray tx (i+1)
            | '{' -> parseAssocArray tx (i+1)
            | '"' -> parseString tx (i+1)
            | 't' | 'f' -> parseBoolean tx i
            | 'n' -> parseNull tx i
            | '-' -> parseNumber tx i
            | ch when System.Char.IsDigit(ch) -> parseNumber tx i
            | ch when System.Char.IsWhiteSpace(ch) -> loop (i+1)
            | ch -> failwith <| sprintf "parse error: [%d] %A" i ch
    loop p
and private parseAssocArray (tx:string) p =
    let rec loop m i =
        if i >= String.length tx then
            failwith "parse error"
        else
            match tx.[i] with
            | '}' -> (i+1, Ob m)
            | ',' -> loop m (i+1)
            | ch when System.Char.IsWhiteSpace(ch) -> loop m (i+1)
            | '"' ->
                    let (j, key) = parseString tx (i+1)
                    let k = tx.IndexOf(':', j)
                    if k < 0 then
                        failwith <| sprintf "parse error: no cron [%d]" j
                    else
                        let (q, value) = parse tx (k+1)
                        loop (Map.add (getStr key) value m) q
            | ch -> failwith <| sprintf "parse error: [%d] %A" i ch
    loop Map.empty p
and private parseArray (tx:string) p =
    let a = new System.Collections.Generic.List<JsonObject>()
    let rec loop i =
        if i >= String.length tx then
            failwith "parse error"
        else
            match tx.[i] with
            | ']' -> (i+1, Ar (a.ToArray()))
            | ch when System.Char.IsWhiteSpace(ch) -> loop (i+1)
            | ',' ->
                    let (j, v) = parse tx (i+1)
                    a.Add(v)
                    loop j
            | _ -> 
                    let (j, v) = parse tx i
                    a.Add(v)
                    loop j
    loop p